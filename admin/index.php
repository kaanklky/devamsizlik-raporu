<!doctype html>
<html>
<head>
	<title>Yönetim | Devamsızlık Raporu</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="./src/css/main.css" />
	<link rel="icon" type="image/png" href="./src/img/favicon.png" />
	<script type="text/javascript" src="./src/js/menu.js"></script>
</head>

<body>
	<div class="loginFrame" id="mainFrame">

		<!-- Basit bir php parola koruması, öyle oturum falan yok. :) -->
		<?php 
			$parola = "******";
			if ($_POST['parola'] != $parola) { 
		?> 
			<div class="loginFrame">
				<form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>"> 
    				<input type="password" title="Şifre" name="parola" />
    				<input type="submit" name="Submit" value="Giriş" />
    			</form> 
    		</div>
		<?php 
			} else { 
				include "baglanti.php";
				echo '
					<script type="text/javascript">
						document.getElementById("mainFrame").className = "mainFrame";
					</script>
				'
		?>
			<div class="menu">
				<a href="#" onclick="devamsizlik()"><div class="menu-sl menu-aktif" id="menu-sl"><p>Devamsızlık Düzenle</p></div></a>
				<a href="#" onclick="ders()"><div class="menu-sg" id="menu-sg"><p>Ders Düzenle</p></div></a>
			</div>
			<div id="devamsizliktab">
				<div class="secFrame"><p>Ders:</p>
					<form method="post" action="guncelle.php">
						<select name="ders">
							<option value="0" selected>Ders Seç:</option>
							<?php
								for ($i = 0; $i <= (count($dersler) - 1); $i++) {
									echo "<option value=\"" . $dersler["ders" . $i]["dersadi"] . "\">" . $dersler["ders" . $i]["dersadi"] . "</option>";
								}
							?>
						</select>
				</div>
				<div class="degistirFrame">
					<p>İşlem:</p>
					<select name="islem">
						<option value="0" selected>İşlem Seç:</option>
						<option value="1">Devamsızlık Ekle</option>
						<option value="2">Devamsızlık Sil</option>
					</select>
					<input type="number" name="deger" value="1" min="1" max="999" step="1" />
					<input type="submit" value="Uygula" />
				</div>
				</form>
			</div>

			<div id="derstab" style="display:none">
				<div class="secFrame"><p>Ders Sil:</p>
					<form method="post" action="sil.php">
						<select name="dersadi">
							<option value="0" selected>Ders Seç:</option>
							<?php
								for ($i = 0; $i <= (count($dersler) - 1); $i++) {
									echo "<option value=\"" . $dersler["ders" . $i]["dersadi"] . "\">" . $dersler["ders" . $i]["dersadi"] . "</option>";
								}
							?>
						</select>
					<input type="submit" value="Sil" />
				</form>
				</div>
				<div class="degistirFrame">
					<form method="post" action="ekle.php">
					<p>Ders Ekle:</p>
					<input type="text" name="dersismi" placeholder="Ders Adı" />
					<input type="number" name="deger" value="1" min="1" max="999" step="1" />
					<input type="text" name="renkkodu" placeholder="Renk kodu (HTML)" />
					<input type="submit" value="Ekle" />
				</form>
				</div>
			</div>

		<?php 
			} 
		?> 
	</div>

	<div class="footerFrame">
		<p>Bu panel Kaan Kölköy tarafından, Devamsızlık Raporu projesini daha kolay yönetmek için yapılmıştır.</p>
	</div>


</body>
</html>