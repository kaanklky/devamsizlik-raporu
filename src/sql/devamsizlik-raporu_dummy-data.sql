# Dump of table devamsizliktablo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `devamsizliktablo`;

CREATE TABLE `devamsizliktablo` (
  `dersadi` text CHARACTER SET utf8,
  `haftalikders` int(1) DEFAULT NULL,
  `devamsizlikders` int(2) DEFAULT NULL,
  `renkkodu` text COLLATE utf8_turkish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

LOCK TABLES `devamsizliktablo` WRITE;
/*!40000 ALTER TABLE `devamsizliktablo` DISABLE KEYS */;

INSERT INTO `devamsizliktablo` (`dersadi`, `haftalikders`, `devamsizlikders`, `renkkodu`)
VALUES
	('Ataturk Ilke ve Inkilaplari',8,0,'\"#FF800D\"'),
	('Matematik',12,5,'\"#44B4D5\"'),
	('Turk Dili',8,4,'\"#03EBA6\"'),
	('Cografya',8,4,'\"#DF412F\"');

/*!40000 ALTER TABLE `devamsizliktablo` ENABLE KEYS */;
UNLOCK TABLES;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
