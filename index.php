<!doctype html>
<html>
<head>
	<meta charset='utf-8'>
	<link rel="icon" type="image/png" href="./src/img/favicon.png" />
	<title>Kaan Kölköy'ün Devamsızlık Raporu</title>
	<link rel="stylesheet" type="text/css" href="./src/css/main.css" />
	<script type="text/javascript" src="./src/js/chart.js"></script>
	<?php include 'data.php'; ?>
    </head>
<body>
	<a href="https://github.com/kaanklky/devamsizlik-raporu" target="_blank"><img class="fork" src="http://dl.kaankolkoy.net/fork.png" /></a>

	<div class="giris">
		<div class="aciklama"><p>Merhaba, adım Kaan Kölköy. Çanakkale Onsekiz Mart Üniversitesi, Bilgisayar Programcılığı bölümünde öğrenciyim.<br/>
		Bu projeyi küçük bir çalışma olarak yaptım, bir <a href="http://www.wikiwand.com/tr/Kata" target="_blank">kata</a> olarak düşünebilirsiniz.
		Görmüş olduğunuz sayfa benim devamsızlık raporumu göstermektedir.
		Yönetim paneli de var! :)<br/><br/>
		Kullanılan teknolojiler: <a href="http://www.wikiwand.com/tr/PHP" target="_blank">PHP</a>, <a href="http://www.wikiwand.com/tr/MySQL" target="_blank">MySQL</a>, <a href="http://www.wikiwand.com/tr/JavaScript" target="_blank">JavaScript</a>, <a href="http://www.wikiwand.com/tr/JSON" target="_blank">JSON</a>, <a href="http://chartjs.org" target="_blank">Chart.JS</a>.</p></div>
	</div>

	<div class="ders-hatti">
	<?php
		for ($i = 0; $i <= (count($dersler) - 1); $i++) { 
			echo "\t\t<div class=\"dv-blok\"><p>" . $dersler["ders" . $i]["dersadi"] . "</p><canvas id=\"" . "ders" . $i . "\"></canvas></div> \n";
		}
	?>
	</div>

	<script type="text/javascript">
	<?php
		for ($i = 0; $i <= (count($dersler) - 1); $i++) {
			echo "\n\tvar " . "ders" . $i . "data = [";
			echo "\n\t\t{\n\t\t\tvalue : " . $dersler["ders" . $i]["devamsizlikders"] . ",\n\t\t\tcolor : " . $standartrenkkodu . ",\n\t\t\tlabel : \"" . $devamsizsaat . "\"\n\t\t},";
			echo "\n\t\t{\n\t\t\tvalue : " . $dersler["ders" . $i]["kalanders"] . ",\n\t\t\tcolor : " . $dersler["ders" . $i]["renkkodu"] . ",\n\t\t\tlabel : \"" . $kalansaat . "\"\n\t\t}";
			echo "\n\t];\n";
		}
 	?>

 	var devamsizlikOptions = {
		segmentShowStroke : true,
		segmentStrokeColor : "#FFF",
		segmentStrokeWidth : 1,
		percentageInnerCutout : 0,
		animationSteps : 100,
		animationEasing : "easeOutBounce",
		animateRotate : true,
		animateScale : false,
		maintainAspectRatio: false,
		responsive : true
	}
		<?php 
			for ($i = 0; $i <= (count($dersler) - 1); $i++) {
				echo "var ders" . $i . "devamsizlik = document.getElementById(\"ders" . $i . "\").getContext(\"2d\"); ";
				echo "new Chart(ders" . $i . "devamsizlik).Pie(ders" . $i . "data, devamsizlikOptions);";
				echo "\n\t\t";
			}
		?>
 	</script>
</body>
</html>
