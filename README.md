Devamsızlık Raporu
============================

Devamsızlık Raporu, okulda devamsızlığımı daha rahat kontrol etmem için oluşturduğum küçük bir projedir.

Kullanılan teknolojiler: [PHP][1], [MySQL][2], [JavaScript][3], [JSON][4], [Chart.JS][5].

> Devamsızlık Raporu, gerekli verileri MySQL veritabanından çekerek PHP ile JSON formatında yazdırır, Chart.JS ile JSON formatındaki veriler pasta grafiğine dönüştürülerek HTML5 Canvas kullanarak ekrana yansıtır.

> Yönetim paneli kısmında ise, basit bir şekilde MySQL üzerindeki verileri yapılan işlem doğrultusunda arttırır veya azaltır.

Yapılacaklar
---

 * Her form girişi için JavaScript ile RegExp yazılacak.

SQL Kullanımı
---

*./src/sql/devamsizlik-raporu_dummy-data.sql* dosyasını veritabanınıza ekledikten sonra, *data.php* dosyasında gerekli değişiklikleri yaparak kullanmaya başlayabilirsiniz.

Sürüm
----

1.4


Lisans
----

GPLv3


**Software is like sex, it's better when it's free!**

[1]:http://www.wikiwand.com/tr/PHP
[2]:http://www.wikiwand.com/tr/MySQL
[3]:http://www.wikiwand.com/tr/JavaScript
[4]:http://www.wikiwand.com/tr/JSON
[5]:http://chartjs.org/
